#  iam/main.tf

resource "aws_iam_role" "role" {
  for_each           = var.roles
  name               = each.value.role.name
  description        = each.value.role.description
  assume_role_policy = each.value.role.assume_role_policy
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

resource "aws_iam_policy" "role_policy" {
  for_each    = var.roles
  name        = each.value.policy.name
  description = each.value.policy.description
  policy      = each.value.policy.policy
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
  for_each   = var.roles
  role       = aws_iam_role.role[each.key].name
  policy_arn = aws_iam_policy.role_policy[each.key].arn
}

resource "aws_iam_instance_profile" "profile" {
  for_each = var.profiles
  name     = each.value.name
  role     = aws_iam_role.role[each.key].name
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

resource "aws_iam_user" "user" {
  for_each = var.users
  name = each.value.name
  path = each.value.path

  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

resource "aws_iam_access_key" "user" {
  for_each = var.users
  user = aws_iam_user.user[each.key].name
}

resource "aws_iam_policy" "user_policy" {
  for_each    = var.users
  name        = each.value.policy.name
  description = each.value.policy.description
  policy      = each.value.policy.policy
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}
resource "aws_iam_user_policy_attachment" "policy_attachment" {
  for_each   = var.users
  user       = aws_iam_user.user[each.key].name
  policy_arn = aws_iam_policy.user_policy[each.key].arn
}
