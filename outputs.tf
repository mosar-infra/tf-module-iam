# iam/outputs.tf

output "role" {
  value = aws_iam_role.role
}

output "role_policy" {
  value = aws_iam_policy.role_policy
}

output "role_policy_attachment" {
  value = aws_iam_role_policy_attachment.policy_attachment
}

output "profile" {
  value = aws_iam_instance_profile.profile
}

output "user" {
  value = aws_iam_user.user
}

output "user_policy" {
  value = aws_iam_policy.user_policy
}

output "user_policy_attachment" {
  value =  aws_iam_user_policy_attachment.policy_attachment
}

output "access_key" {
  value = aws_iam_access_key.user
  sensitive = true
}
